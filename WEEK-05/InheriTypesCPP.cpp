//C++ Program to demonstrate the usage of types of inheritences
#include<iostream>
using namespace std;
class GrandMother //base class
{
  public:
  GrandMother()
  {
      cout << "GrandMother() is called..." << endl;
  }
};
//single inheritence
class Father : public GrandMother //Fther class inheriting the properties of GrandMother class
{
    public:
    Father()
    {
     cout << "Father() inherits from GrandMother()..." << endl;   
    }
};
class Mother //base class
{
    public:
    Mother()
    {
        cout << "Mother() is called..." << endl;
    }
};
//multiple inheritence
class Son : public Father,public Mother //Son class inherits the properties of both father and mother classes  
{};
//multilevel inheritence
class Daughter : public Father
{
    public:
    Daughter()
    {
        cout << "Daughter() inherits from Father()..." << endl;
    }
};
//hierarchial inheritence
class Brother : public Mother{}; //Brother class is derived from the base class mother
class Sister : public Mother{};  //sister class is derived from the base class mother
class GrandFather  //Base class
{
  public:
  GrandFather()
  {
      cout << "GrandFather() is called..." << endl;
  }
};
//hybrid inheritance
class GrandDaughter : public Father, public GrandFather{}; //Grand daughter is derived from father and grandfather classes
class Baby : public GrandDaughter{};  // baby is derived from the GrandDaughter class
//main function
int main()
{
    cout << "single inheritence" << endl;
   Father obj;
    cout << "multiple inheritence" << endl;
    Son obj1;
    cout << "multilevel inheritence" << endl;
    Daughter obj2;
    cout << "Hierarchial inheritence" << endl;
    Brother obj3;
    Sister obj4;
    cout << "Hybrid inheritence" << endl;
    GrandDaughter obj5;
    Baby obj6;
    return 0;
}