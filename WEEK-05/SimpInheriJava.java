//Java Program to demonstrate simple inheritence
import java.util.*;
  class Operators
{
  int a=7;
}
class Arithmetic extends Operators
{
  int b= 5;
  }
  class SimpInheriJava
{
  public static void main(String args[])
  {
    Arithmetic k = new Arithmetic();
    System.out.println("Addition of two numbers : "+ (k.a + k.b));
    System.out.println("Difference of two numbers : "+ (k.a - k.b));
    System.out.println("product of two numbers : "+ (k.a * k.b));
    System.out.println("quotient of two numbers : "+ (k.a / k.b));
    System.out.println("Remainder of two numbers: "+ (k.a % k.b));
  }
}