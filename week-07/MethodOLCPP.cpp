//C++ program to demonstrate overloading of methods
#include<iostream>
using namespace std;
class M_Overload
{
 public:
 string fullName,collegeName;
 int Num;
 double semPercentage;
 void student(string name) //Student() with string as a parameter
 {
   fullName = name;
 }
 void student(int num) //student() with integer as a parameter
 {
   Num = num;
 }
 void student(string cName,double semper)//student() with string and double as a parameters
 {
   collegeName=cName;
   semPercentage=semper;
 }
 void display()
 {
     cout << "Full Name: " << fullName << endl;
     cout << "Roll Number: " << Num << endl;
      cout << "college name: " << collegeName << endl;
       cout << "Sem percentage: " << semPercentage << endl;
 }
};
int main()
{
    string name,cName;
    int num;
    double semper;
    cout << "Enter your name: " << endl;
    cin >> name;
    cout << "Enter your Roll number: " << endl;
    cin >> num;
    cout << "Enter your college name and sem percentage : " <<endl;
    cin >> cName >> semper ;
    M_Overload obj; // creating object for M_overload class
    obj.student(name); // calling a function with string as a parameter
    obj.student(num); // calling a function with integer as a parameter
    obj.student(cName,semper); // calling a function with string and double as a parameter
    obj.display();  // calling the display function to display the required contents
    return 0;
}