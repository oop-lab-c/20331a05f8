//C++ Program to demonstrate pure Abstraction
#include <iostream>
using namespace std;
class Animal
{
  public:
    virtual void sound()=0; // Do-Nothing Function
};
class Dog : public Animal
{
  public:
  void sound() // sound of Dog
  {
    cout << "Bow - Bow" << endl;
  }
};
class Cat : public Animal
{
  public:
  void sound() //Sound of Cat
  {
    cout << "Meow - Meow" << endl;
  }
};
int main()
{
  Dog d;
  Cat c;
  d.sound();
  c.sound();
  return 0;
}