//Java Program to demonstrate the partial abstraction
import java.util.*;
abstract class Animal // abstract class
{
  abstract void sound(); // abstract method
  void action()
  {
    System.out.println("Shouting");
  }
}
class Dog extends Animal
{
  void sound()
  {
    System.out.println("Bow-Bow");
  }
}
class Cat extends Animal
{
  void sound()
  {
    System.out.println("Meow-Meow");
  }
}
public class ParAbsJava
{
    public static void main(String args[])
    { 
    Dog d = new Dog(); // creating object for dog class
    Cat c= new Cat(); // creating object for cat class
    d.action(); // calling action() by using the object of dog class
    d.sound(); // calling sound() by using the object of dog class
    c.action(); // calling action() by using the object of cat class
    c.sound();  // calling sound() by using the object of cat class
    }   
}
