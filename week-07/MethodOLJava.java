//Program to demonstrate overloading of methods
import java.util.*;
public class MethodOLJava 
{
    int add(int n1,int n2,int n3) // function definition of add() with three parameters of same type
    {
        return n1+n2+n3;
    }
    int add(int n1,int n2) //function definition of add() with two parameters of same type
    {
        return n1+n2;
    }
    public static void main(String[] args)
    {
        MethodOLJava obj = new MethodOLJava(); // creating object for the class
        Scanner input = new Scanner(System.in); // To take and read the input from the user
        System.out.println("Enter the values : ");
        int n1 = input.nextInt();
        int n2 = input.nextInt(); // To take integer as an input from the user
        int n3 = input.nextInt();
        System.out.println("Addition of two numbers : " + obj.add(n1,n2));
        System.out.println("Addition of three numbers : " + obj.add(n1,n2,n3));
    }
}
