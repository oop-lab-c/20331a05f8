// Java program to demonstrate pure abstraction using interfaces
import java.util.*;
interface Animal // interface of a animal
{
  void sound();
}
class Dog implements Animal // Dog class implements animal interface
{
  public void sound()
  {
    System.out.println("Bow - Bow!!");
  }
}
class Cat implements Animal //cat class implements animal interface
{
  public void sound()
  {
    System.out.println("Meow - Meow!!");
  }
}
public class PureAbsJava
{
    public static void main(String args[])
    { 
     Dog d = new Dog(); //creating object for dog class
     Cat c = new Cat(); //creating object for cat class
     d.sound(); // calling sound() by using the object of dog class
     c.sound(); // calling sound() by using the object of cat class
    }   
}
