//Program to demonstrate the template functions and classes
#include<iostream>
using namespace std;
template <typename A>  // template function
A Add(A num1, A num2)
{
return (num1+num2);
}
template <class T>    // template class
class Number
{
    T num;
    public:
    Number(T n) : num(n) {} // constructor
    T getNum()
    {
        return num;
    }

};
int main()
{
    int res1,n1,n2,x;
    double res2,n3,n4,y;
    cout << "Enter integer values: " << endl;
    cin >> n1 >> n2 >> x ;
    cout << "Enter Double values : " << endl;
    cin >> n3 >> n4 >> y;
    res1 = Add <int>(n1,n2); // calling the function with int type
    res2 = Add <double>(n3,n4); //calling the function with double type
    cout << "Result1 : " << res1 << " "<< "Result2 : " << res2 << endl;
    Number<int> numberInt(x);    // create object with int type
    Number<double> numberDouble(y);  // create object with double type
    cout << "int Number = " << numberInt.getNum() << endl;
    cout << "double Number = " << numberDouble.getNum() << endl;
    return 0;
}