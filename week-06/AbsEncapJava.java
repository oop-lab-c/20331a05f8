//Java Program to access member variables using setvar() and getVar()
import java.util.*;
class AccessSpecifierDemo
{
    private int priVar;    //private variable
    public int pubVar;     //public variable
    protected int proVar;  //protected variable
    public void setVar(int priValue,int pubValue, int proValue)   // setVar() is used for setting the values from the user
    {
      priVar = priValue;
      pubVar = pubValue;
      proVar = proValue;
    }
    public void getVar()  //getVar() is used to get the values which have been set
    {
        System.out.println("Private value : " + (priVar));
        System.out.println("Public value : " + (pubVar));
        System.out.println("Protected value : " + (proVar));
    }
}
public class AbsEncapJava
{
  public static void main(String[] args)
  {
      AccessSpecifierDemo obj = new AccessSpecifierDemo();
      Scanner input = new Scanner(System.in);   //reading input from the user
      System.out.println("Enter private value :");
      int pri = input.nextInt();
      System.out.println("Enter public value :");
      int pub = input.nextInt();
      System.out.println("Enter protected value :");
      int pro = input.nextInt();
      obj.setVar(pri,pub,pro);
      obj.getVar();
  }   
}
